package pl.training.cloud.catalog.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@RequiredArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Product {

    @GeneratedValue
    @Id
    private Long id;
    @NonNull
    private String name;
    @NonNull
    private BigDecimal price;

}
