package pl.training.cloud.catalog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.training.cloud.catalog.model.Product;

import java.util.Optional;

public interface CatalogRepository extends JpaRepository<Product, Long> {

    Optional<Product> getById(Long id);

}
