package pl.training.cloud.catalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatalogMicroservice {

	public static void main(String[] args) {
		SpringApplication.run(CatalogMicroservice.class, args);
	}

}
