package pl.training.cloud.catalog.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.training.cloud.catalog.mock.ProductCatalogMock;
import pl.training.cloud.catalog.model.Product;
import pl.training.cloud.catalog.repository.CatalogRepository;

@RequiredArgsConstructor
@Service
public class CatalogService {

    @NonNull
    private CatalogRepository catalogRepository;

    public Product getProductById(Long id) {
        return ProductCatalogMock.getById(id)
                .orElseThrow(ProductNotFoundException::new);

//        return catalogRepository.getById(id)
//                .orElseThrow(ProductNotFoundException::new);
    }

}
