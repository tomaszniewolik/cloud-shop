package pl.training.cloud.catalog.controller;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.training.cloud.catalog.dto.ProductDto;
import pl.training.cloud.catalog.model.Product;
import pl.training.cloud.catalog.model.Mapper;
import pl.training.cloud.catalog.service.CatalogService;

import java.net.URI;

@RequestMapping("products")
@RestController
@RequiredArgsConstructor
public class CatalogController {

    @NonNull
    private CatalogService catalogService;

    @NonNull
    private Mapper mapper;

    private UriBuilder uriBuilder = new UriBuilder();

    @RequestMapping(value = "{product-id}", method = RequestMethod.GET)
    public ProductDto getProductById(@PathVariable("product-id") Long id) {
        Product product = catalogService.getProductById(id);
        return modelToDto(product);
    }

    private ProductDto modelToDto(Product product) {
        return mapper.map(product, ProductDto.class);
    }

    private Product dtoToModel(ProductDto ProductDto) {
        return mapper.map(ProductDto, Product.class);
    }

}
