package pl.training.cloud.catalog.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExceptionDto {

    private String description;

}