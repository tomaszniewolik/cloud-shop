package pl.training.cloud.catalog.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {

    private String name;
    private Long id;
    private BigDecimal price;

}
