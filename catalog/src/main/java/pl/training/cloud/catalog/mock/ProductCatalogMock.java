package pl.training.cloud.catalog.mock;

import pl.training.cloud.catalog.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductCatalogMock {

    private static List<Product> products = new ArrayList<Product>();

    public static Optional<Product>  getById(Long id) {
        initialize();
        return products.stream().filter(product -> product.getId().equals(id)).findFirst();
    }

    public static List<Product>  get() {
        initialize();
        return products;
    }

    private static void initialize() {
        if (products.size()==0) {
            Product product = new Product("Zeszyt w linię", BigDecimal.valueOf(7.5));
            products.add(product);
            product = new Product("Zeszyt w kratkę", BigDecimal.valueOf(8.5));
            products.add(product);
            product = new Product("Blok rysunkowy", BigDecimal.valueOf(17.5));
            products.add(product);
            product = new Product("Kredki", BigDecimal.valueOf(9.5));
            products.add(product);
            product = new Product("Ołówek", BigDecimal.valueOf(4.5));
            products.add(product);
        }
    }
}
